#!/opt/gfa/python-3.5/latest/bin/python 

# server to check status of bunch compressor and synchronize the values 

from datetime import datetime

import time
import queue
import threading
import logging

#  modules for interface with epics and the softIOC package
from pcaspy import Driver, SimpleServer, Alarm, Severity
import sys
import socket
from epics import caget,caput,PV

import numpy as np

# program specific modules - transformation between moto position etc.


# the channels for the server

prefix = 'SFBD-MEAS:'
pvdb = {

# general 
    'PING'      : {'type': 'string', 'scan' : 1,},
    'STOP'      : {'type': 'int'}
}

bun=['BUNCH1','BUNCH2']

# for emittances
channels = {'EMITTANCE':'nm','ALPHA':'','BETA':'m','MISMATCH':''}
planes=['X','Y']
loc = ['LH','BC1','BC2','ARCOL','ARDUMP','ATHOS','LINAC1','LINAC3','SY']

for e1 in channels.keys():
    for e2 in planes:
        for e3 in loc:
            for e4 in bun:
                PV='%s-%s-%s-%s' % (e1,e2,e3,e4)
                unit=channels[e1]
                if unit is '':
                    pvdb[PV]={'prec':3,'type':'float','count':1,'value':-1}   
                else:
                    pvdb[PV]={'prec':3,'type':'float','count':1,'value':-1,'unit':unit}   


# for slice emittances
channels = {'EMITTANCE':'nm','ALPHA':'','BETA':'m','MISMATCH':'','PROFILE':''}
planes=['X']
loc = ['BC1','ARCOL']

for e1 in channels.keys():
    for e2 in planes:
        for e3 in loc:
            for e4 in bun:
                PV='%s-%s-SLICE-%s-%s' % (e1,e2,e3,e4)
                unit=channels[e1]
                if unit is '':
                    pvdb[PV]={'prec':3,'type':'float','count':200,'value':-1}   
                else:
                    pvdb[PV]={'prec':3,'type':'float','count':200,'value':-1,'unit':unit}   

# for profile measurements (current and energy spread)
channels = {'CURRENT':'A','TIME': 'fs','ENERGY':'GeV','ENERGYSPREAD':'keV'}
loc = ['BC1','ARCOL']

for e1 in channels.keys():
    for e2 in loc:
        for e3 in bun:
            PV='%s-%s-%s' % (e1,e2,e3)
            unit=channels[e1]
            if unit is '':
                pvdb[PV]={'prec':3,'type':'float','count':200,'value':-1}   
            else:
                pvdb[PV]={'prec':3,'type':'float','count':200,'value':-1,'unit':unit}   

 
class BDMeasurementServer(Driver):
    def  __init__(self):

        super(BDMeasurementServer, self).__init__()
        
        self.version='1.1.1'
        self.program='SF-MeasurementServer'

        # enable logging
        self.logfilename="/sf/data/applications/BD-MeasurementServer/MeasurementServer.log"
        logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M:%S',
                    filename=self.logfilename,
                    filemode='w')

        self.logger=logging.getLogger(self.program)
        self.logger.info('started at %s' % datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self.logger.info('Version: %s ' % self.version)
        self.logger.info('Host: %s' % socket.gethostname())
        self.running=True
        
        

#----------------------------------------
# event haendler for read and write
 
    def read(self, reason):
        if reason =='PING':
            return datetime.now().strftime('Alive at %Y-%m-%d %H:%M:%S')

        value = self.getParam(reason)
        return value

 
    def write(self,reason,value):
 
        if reason == 'STOP':
           if value > 0: 
               self.running=False
        self.logger.info('Updated Measurement %s with value %s at %s' % (reason, str(value), datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        self.setParam(reason,value)
 
 
    def disconnect(self):
        self.logger.info('terminated at %s' % datetime.now().strftime('%Y-%m-%d %H:%M:%S)'))

    def __del__(self):
        self.disconnect()






if __name__ == '__main__':
    server = SimpleServer()
    server.createPV(prefix, pvdb)
    driver = BDMeasurementServer()

    # process CA transactions
    while driver.running:
        try:
            server.process(0.1)
        except KeyboardInterrupt:
            sys.exit()





