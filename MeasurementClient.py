#!/opt/gfa/python-3.5/latest/bin/python 

import sys
import datetime
import os.path
import time,datetime
from os import walk
import numpy as np
import sys
import re

from PyQt4 import QtGui,QtCore
from PyQt4.uic import loadUiType
#from PyQt4.QtCore import *

from matplotlib.figure import Figure
import matplotlib.patches as patches

from matplotlib.backends.backend_qt4agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)

Ui_MeasurementClientGUI, QMainWindow = loadUiType('/sf/bd/server/MeasurementServer/current/MeasurementClient.ui')

# PV interface

from epics import PV,caput,caget



class MeasurementClient(QMainWindow,Ui_MeasurementClientGUI):
    def __init__(self,):
        super(MeasurementClient,self).__init__()
        self.setupUi(self)
        self.initmpl()
        self.Layout.setScaledContents(True)
        self.Layout.setPixmap(QtGui.QPixmap("/sf/bd/server/MeasurementServer/v1.0.0/Layout.png"))

        self.Refresh.clicked.connect(self.PVevent)
        self.ProfileARCOL.currentIndexChanged.connect(self.PVevent)
        self.ProfileBC1.currentIndexChanged.connect(self.PVevent)
        self.ProfileAT.currentIndexChanged.connect(self.PVevent)

        self.font = {'family': 'sans-serif','color':  'darkred','weight': 'bold','size': 16 }

#       setting up the PV channels
        loc={'LH','BC1','ARCOL'}
        channels={'EMITTANCE','MISMATCH'}
        planes={'X','Y'}
        
        self.PV=[]
        for e1 in loc:
            for e2 in channels:
                for e3 in planes:
                    pv=PV(pvname='SFBD-MEAS:%s-%s-%s' % (e2,e3,e1))
                    pv.add_callback(self.PVevent)
                    self.PV.append(pv)
                    
        self.PVping=PV('SFBD-MEAS:PING')

        self.PVprof={}
        self.PVtime={}
        self.PVenergy={}
        self.PVspread={}
        self.PVemit={}
        self.PVmis={}
        self.PVdist={}
        loc={'BC1','ARCOL'}
        for e1 in loc:
            self.PVprof[e1]=PV(pvname='SFBD-MEAS:CURRENT-%s' % e1)
            self.PVtime[e1]=PV(pvname='SFBD-MEAS:TIME-%s' % e1)
            self.PVenergy[e1]=PV(pvname='SFBD-MEAS:ENERGY-%s' %e1)
            self.PVspread[e1]=PV(pvname='SFBD-MEAS:ENERGYSPREAD-%s' % e1)
            self.PVemit[e1]=PV(pvname='SFBD-MEAS:EMITTANCE-X-SLICE-%s' % e1)
            self.PVmis[e1]=PV(pvname='SFBD-MEAS:MISMATCH-X-SLICE-%s' % e1)
            self.PVdist[e1]=PV(pvname='SFBD-MEAS:PROFILE-X-SLICE-%s' % e1)

#        self.generateProfile()
        


#       setting timer
        self.enforceUpdate=True
        self.timer=QtCore.QTimer(self)
        self.connect(self.timer,QtCore.SIGNAL("timeout()"),self.QTimerEvent)
        self.timer.start(1000)


    def generateProfile(self):
        N=100
        sigt=500
        t=np.linspace(-2000,2000,N)
        x=155*np.exp(-0.5*t*t/sigt/sigt)
        PVloc=PV(pvname='SFBD-MEAS:CURRENT-BC1')
        PVloc.put(x)
        PVloc=PV(pvname='SFBD-MEAS:TIME-BC1')
        PVloc.put(t)
        e=np.linspace(5.32,5.33,N)
        s=np.linspace(150,800,N)
        PVloc=PV(pvname='SFBD-MEAS:TIME-ARCOL')
        PVloc.put(t/20)
        PVloc=PV(pvname='SFBD-MEAS:ENERGY-ARCOL')
        PVloc.put(e)
        PVloc=PV(pvname='SFBD-MEAS:ENERGYSPREAD-ARCOL')
        PVloc.put(s)
        
        N=20
        t=np.linspace(-3,3,N)
        x=np.exp(-0.5*t*t)
        m=np.linspace(1.1,1.3,N)
        e=np.linspace(230,240,N)+50*x
        PVloc=PV(pvname='SFBD-MEAS:EMITTANCE-X-SLICE-BC1')
        PVloc.put(e)
        PVloc=PV(pvname='SFBD-MEAS:MISMATCH-X-SLICE-BC1')
        PVloc.put(m)
        PVloc=PV(pvname='SFBD-MEAS:PROFILE-X-SLICE-BC1')
        PVloc.put(x)
        

    def energyProfile(self,loc,d,axes,canvas):
        axes.clear()
        x=self.PVtime[loc].get()
        y1=self.PVenergy[loc].get()
        y2=self.PVspread[loc].get()
        if np.max(y1) < 0 or (np.max(x)-np.min(x)) <=0:
            axes.set_xlim(0,1)
            axes.set_ylim(0,1)
            axes.text(0.5,0.5,'No Data', horizontalalignment='center',verticalalignment='center',fontdict=self.font)          
            canvas.draw()
            return
        xlab='t (fs)'
        if (np.max(x)-np.min(x)) > 1000:
            x=x/1000
            xlab='t (ps)'
        yup=y1+3e-6*y2
        ylo=y1-3e-6*y2
        ymean=np.mean(y1)
        y1 =(y1 -ymean)/ymean*100
        yup=(yup-ymean)/ymean*100
        ylo=(ylo-ymean)/ymean*100

        ylab='dE (%)'        
        time= (d-self.PVenergy[loc].timestamp)/86400.            
        color1='g'
        color2='g--'
        if time > 1:
            color1='b'
            color2='b--'
        if time > 7:
            color1='r'
            color2='r--'
        axes.plot(x,y1,color1,x,yup,color2,x,ylo,color2)
        axes.set_xlabel(xlab)
        axes.set_ylabel(ylab)
        canvas.draw()


    def currentProfile(self,loc,d,axes,canvas):
        axes.clear()
        x=self.PVtime[loc].get()
        y=self.PVprof[loc].get()
        if np.max(y) < 0 or (np.max(x)-np.min(x)) <=0:
            axes.set_xlim(0,1)
            axes.set_ylim(0,1)
            axes.text(0.5,0.5,'No Data', horizontalalignment='center',verticalalignment='center',fontdict=self.font)          
            canvas.draw()
            return

        xlab='t (fs)'
        if (np.max(x)-np.min(x)) > 1000:
            x=x/1000
            xlab='t (ps)'
        ylab='I (A)'
        if np.max(y) > 1000:
            y=y/1000
            ylab = 'I (kA)'
        time= (d-self.PVprof[loc].timestamp)/86400.            
        color='g'
        if time > 1:
            color='b'
        if time > 7:
            color='r'
        axes.plot(x,y,color)
        axes.set_xlabel(xlab)
        axes.set_ylabel(ylab)
        canvas.draw()

    def sliceProfile(self,loc,d,axes,canvas,mode):
        axes.clear()
        if mode is 0:
            y=self.PVemit[loc].get()
            ylab='emittnce (nm)'
        else:
            y=self.PVmis[loc].get()
            ylab='mismatch'

        p=self.PVdist[loc].get()

        if np.max(y) < 0:
            axes.set_xlim(0,1)
            axes.set_ylim(0,1)
            axes.text(0.5,0.5,'No Data', horizontalalignment='center',verticalalignment='center',fontdict=self.font)          
            canvas.draw()
            return
        maxy=np.max(y)
        maxp=np.max(p)
        p=maxy*0.8*p/maxp

        time= (d-self.PVemit[loc].timestamp)/86400.            
        color='g'
        if time > 1:
            color='b'
        if time > 7:
            color='r'
        axes.plot(y,color)
        axes.plot(p,color+'.')
        axes.set_xlabel('#slice')
        axes.set_ylabel(ylab)
        canvas.draw()

    def updateProfile(self,loc,d,axes,canvas):

        wid='Profile%s' % loc
        if wid in self.__dict__.keys():
            idx=self.__dict__[wid].currentIndex()
            if idx is 0:
               self.currentProfile(loc,d,axes,canvas) 
            if idx is 1:
               self.energyProfile(loc,d,axes,canvas) 
            if idx is 2:
                self.sliceProfile(loc,d,axes,canvas,0)
            if idx is 3:
                self.sliceProfile(loc,d,axes,canvas,1)



    def updatePanel(self):

        d=datetime.datetime.now().timestamp()
        for p in self.PV:
            val = p.value
            time= (d-p.timestamp)/86400.
            ch  = p.pvname
            fld=ch.split(':')
            wid=fld[1].replace("-","")
            if wid in self.__dict__.keys():
                self.__dict__[wid].setText('%6.2f (%dd)' % (val,time))
                if val < 0:
                    self.__dict__[wid].setStyleSheet("color: white;")
                elif time < 1:
                    self.__dict__[wid].setStyleSheet("color: green;")
                elif time < 7:
                    self.__dict__[wid].setStyleSheet("color: blue;")
                else:
                    self.__dict__[wid].setStyleSheet("color: red;")

        self.updateProfile('BC1',d,self.axesIN,self.canvasIN)
        self.updateProfile('ARCOL',d,self.axesAR,self.canvasAR)

        self.enforceUpdate=False



    def initmpl(self):
        self.figAR=Figure()
        self.axesAR=self.figAR.add_subplot(111,position=[0.15,0.17,0.8,0.75])
        self.canvasAR = FigureCanvas(self.figAR)
        self.mplvlAR.addWidget(self.canvasAR)
        self.canvasAR.draw()

        self.figAT=Figure()
        self.axesAT=self.figAT.add_subplot(111,position=[0.15,0.17,0.8,0.75])
        self.canvasAT = FigureCanvas(self.figAT)
        self.mplvlAT.addWidget(self.canvasAT)
        self.canvasAT.draw()

        self.figIN=Figure()
        self.axesIN=self.figIN.add_subplot(111,position=[0.15,0.17,0.8,0.75])
        self.canvasIN = FigureCanvas(self.figIN)
        self.mplvlIN.addWidget(self.canvasIN)
        self.canvasIN.draw()


    def PVevent(self,pvname=None,char_value=None,**kw):
        self.enforceUpdate=True                                
                                    

    def QTimerEvent(self):     # periodic update of system variables
        self.Ping.setText(self.PVping.get())
        if self.enforceUpdate:
            self.updatePanel()

# --------------------------------
# Main routine

if __name__ == '__main__':
    QtGui.QApplication.setStyle(QtGui.QStyleFactory.create("plastique"))
    app=QtGui.QApplication(sys.argv)
    main=MeasurementClient()
    main.show()
    sys.exit(app.exec_())
