import MeasurementServer as MS


filename='S_SFBD_MEASSERVER.config'

ST=1
MT=60
LT=600

f=open(filename,'w')
f.write('#--------------------------------------------------------\n')
f.write('# pycaspy channels for SwissFEL BD Measurement Server Channels\n') 
f.write('# contact: Sven Reiche \n\n')
for key in MS.pvdb.keys():
    if 'PING' in key:
        continue
    f.write('%s%s %d %d %d\n' % (MS.prefix,key,ST,MT, LT))
f.close()

